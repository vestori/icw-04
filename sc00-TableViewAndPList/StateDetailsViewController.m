//
//  StateDetailsViewController.m
//  sc00-TableViewAndPList
//
//  Created by Orlando Gotera on 11/7/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "StateDetailsViewController.h"


@interface StateDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *stateName;
@property (weak, nonatomic) IBOutlet UILabel *capitalName;
@property (weak, nonatomic) IBOutlet UILabel *motto;
@property (weak, nonatomic) IBOutlet UILabel *population;
@property (weak, nonatomic) IBOutlet UILabel *founded;
@property (weak, nonatomic) IBOutlet UIImageView *flagImage;



@end

@implementation StateDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.stateName.text = self.myStateName;
    self.capitalName.text = self.myStateCapital;
    self.motto.text = self.myStateMotto;
    self.population.text = self.myStatePopulation;
    self.founded.text = self.myStateFounded;
    self.flagImage.image = [UIImage imageNamed:self.myStateFlag];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

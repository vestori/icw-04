//
//  NamesTableViewController.m
//  sc00-TableViewAndPList
//
//  Created by Orlando Gotera on 11/6/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "StatesTableViewController.h"
#import "StateDetailsViewController.h"
#import "StatesTableViewCell.h"

@interface StatesTableViewController ()

@property (nonatomic, strong) NSDictionary *states;
@property (nonatomic, strong) NSArray *keys;

@end

@implementation StatesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //get path to the plist from the bundle
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"USStates" ofType:@"plist"];
    
    //grab contents of plist and save it to our dictionary
    
    self.states = [NSDictionary dictionaryWithContentsOfFile:path];
    
    //NSLog(@"%@", self.states[@"Alabama"][1]);
    
    //each key is going to be a section
    //sort keys
    self.keys = [[self.states allKeys]sortedArrayUsingSelector:@selector(compare:)];
    //NSLog(@"%@", self.keys[0]);
    
    //NSLog(@"%@", self.states[@"Alabama"][3]);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSString *key = self.keys[section];
//
//    //for each key get an array
//
//    NSArray *keyValues = self.states[key];
//
//    return [keyValues count];
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"nameCell";
    StatesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
 
    NSString *key = self.keys[indexPath.section];
    NSArray *keyValues = self.states[key];
 
    //assign to cells
    cell.stateName.text = keyValues[0];
    cell.stateCapital.text = keyValues[4];
    cell.stateFlag.image = [UIImage imageNamed:keyValues[2]];
    //self.keys[indexPath.row];
    
    
    return cell;
    
}

//-(NSArray *)sectionIndexTitlesForTableView:(UITableView *) tableView {
//    return self.keys;
//}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    if ([[segue identifier] isEqualToString:@"stateDetails"]) {
          StateDetailsViewController *detailVC = [segue destinationViewController];
          NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
//        StateInfo *item = [self.usStates objectAtIndex:myIndexPath.row];
          NSString *key = self.keys[myIndexPath.section];
          NSArray *keyValues = self.states[key];
          detailVC.myStateName = keyValues[0];
          detailVC.myStateFounded = keyValues[1];
        detailVC.myStateFlag = keyValues[2];
        detailVC.myStateMotto = keyValues[3];
        detailVC.myStateCapital = keyValues[4];
        detailVC.myStatePopulation = keyValues[5];


        //NSLog(@"%@", self.states[@"Alabama"][3]);
        
    }
    // Pass the selected object to the new view controller.
}


@end

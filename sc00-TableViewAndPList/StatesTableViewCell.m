//
//  StatesTableViewCell.m
//  sc00-TableViewAndPList
//
//  Created by user on 11/8/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "StatesTableViewCell.h"

@implementation StatesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

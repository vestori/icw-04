//
//  StatesTableViewCell.h
//  sc00-TableViewAndPList
//
//  Created by user on 11/8/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface StatesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *stateFlag;
@property (weak, nonatomic) IBOutlet UILabel *stateName;
@property (weak, nonatomic) IBOutlet UILabel *stateCapital;

@end
